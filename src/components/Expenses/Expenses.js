import React, { useState } from "react";

import Card from "../Cards/Card";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';

import "./Expenses.css";

const Expenses = (props) => {
  const [yearFilter, setYearFilter] = useState("2021");

  const yearFilterHandler = (selectedYearFilter) => {
    setYearFilter(selectedYearFilter);
  };

  const expenseItemFilter = expense => {
    return expense.date.getFullYear().toString() === yearFilter;
  };

  const filteredExpenses = props.item.filter(expenseItemFilter);

  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter
          selectedYear={yearFilter}
          onYearFilter={yearFilterHandler}
        />
        <ExpensesChart expenses={filteredExpenses}/>
        <ExpensesList items={filteredExpenses}/>
      </Card>
    </div>
  );
};

export default Expenses;
