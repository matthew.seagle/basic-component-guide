import { useState } from 'react';

import ExpenseForm from './ExpenseForm';

import './NewExpense.css';

const NewExpense = (props) => {
    const [isEditing, setIsEditing] = useState(false);

    const startEditingHandler = () => {
        setIsEditing(true);
    };

    const stopEditingHandler = () => {
        setIsEditing(false);
    }

    const saveExpenseDataHandler = (submittedExpenseData) => {
        const expenseData = {
            id: Math.random().toString(),
            ...submittedExpenseData
        };
        props.onAddExpense(expenseData);
        setIsEditing(false);
    };

    return (
        <div className="new-expense">
            {!isEditing && <button onClick={startEditingHandler}>ADD NEW EXPENSE</button>}
            {isEditing && <ExpenseForm onSaveExpenseData={saveExpenseDataHandler} onStopEditing={stopEditingHandler}/>}
        </div>
    );
};

export default NewExpense;