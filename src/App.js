import { useState } from "react";

import Card from "./components/Cards/Card";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";
import "./components/Expenses/Expenses.css";

const DUMMY_EXPENSES = [
  {
    id: Math.random().toString(),
    title: "Motorcycle Payment",
    amount: 183.0,
    date: new Date(2021, 9, 23),
  },
  { id: Math.random().toString(), title: "Rent", amount: 1350.0, date: new Date(2021, 10, 1) },
  {
    id: Math.random().toString(),
    title: "Car Payment",
    amount: 183.0,
    date: new Date(2019, 10, 3),
  }
];

const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = expense => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Card className="expenses">
        <Expenses item={expenses} />
      </Card>
    </div>
  );
};

export default App;
